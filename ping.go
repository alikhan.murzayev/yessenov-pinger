package main

import (
	"fmt"
	"net/http"
	"sync"
)

func main() {
	url := "https://yessenovfoundation.org/o-fonde/programmyi/resursyi/razvitie-it-kompetentsiy/yessenov-data-lab-2020"
	numIters := 1000

	waitGroup := sync.WaitGroup{}
	for i := 0; i < numIters; i++ {
		waitGroup.Add(1)
		pingPage(&waitGroup, url)
	}
	waitGroup.Wait()

}

func pingPage(waitGroup *sync.WaitGroup, url string) {
	defer waitGroup.Done()
	_, _ = http.Get(url)
	fmt.Print("*")
}
